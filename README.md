# cvPerso

This repo hosts my personal cv website accessible at `https://cv.antoinepissot.fr`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4300/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

### Generate component
Run `ng generate component components/container/layout --module app` to create a component within container folder and add it automatically to app.module.ts

Run `ng generate component components/presentation/eduaction --module app` to create a component within presentation folder and add it automatically to app.module.ts

### Generate service
Run `ng generate service _services/logger` to create a new service in `_services` folder

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
