import bugsnag from '@bugsnag/js';

export const environment = {
  production: true,
  DOMAIN: 'cv.antoinepissot.fr',
  CDN_TONIO: 'https://cdn.antoinepissot.fr/',
  BACKEND_API: 'https://api.antoinepissot.fr/',
  BUGSNAG_KEY: '420acbd676c793e34e9a858b90c7b3ad',
  googleAnalyticsKey: 'UA-138509225-1',
  VERSION: require('../../package.json').version,
  TARGET_DEPLOYMENT: 'PROD',
};


export const bugsnagClient = bugsnag( {
  apiKey: environment.BUGSNAG_KEY,
  appVersion: environment.VERSION,
  releaseStage: environment.TARGET_DEPLOYMENT,
  notifyReleaseStages: [ 'PROD' ],
  autoCaptureSessions: true,
} );

export const ENV_PROVIDERS = [
];
