import { Injectable, OnDestroy } from '@angular/core';
import { NgcCookieConsentConfig, NgcCookieConsentService, NgcInitializeEvent, NgcNoCookieLawEvent, NgcStatusChangeEvent } from 'ngx-cookieconsent';
import { LoggerService } from '@app/_services/logger.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@src/environments/environment';
import { AnalyticsService } from '@app/_services/analytics.service';

export const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: environment.DOMAIN
  },
  palette: {
    popup: {
      background: '#000'
    },
    button: {
      background: '#f1d600'
    }
  },
  theme: 'edgeless',
  type: 'opt-out'
};


@Injectable( {
  providedIn: 'root'
} )
export class CookieService implements OnDestroy {

  // keep refs to subscriptions to be able to unsubscribe later
  private popupOpenSubscription: Subscription;
  private popupCloseSubscription: Subscription;
  private initializeSubscription: Subscription;
  private statusChangeSubscription: Subscription;
  private revokeChoiceSubscription: Subscription;
  private noCookieLawSubscription: Subscription;

  constructor(
    private ccService: NgcCookieConsentService,
    private logger: LoggerService,
    private translateService: TranslateService,
    private analytics: AnalyticsService ) {
  }


  public init() {
    this.logger.log( 'CookieService - init()' );

    this.translateService//
    .get( [ 'cookie.header', 'cookie.message', 'cookie.dismiss', 'cookie.allow', 'cookie.deny', 'cookie.link', 'cookie.policy' ] )
    .subscribe( data => {

      this.ccService.getConfig().content = this.ccService.getConfig().content || {};
      // Override default messages with the translated ones
      this.ccService.getConfig().content.header = data[ 'cookie.header' ];
      this.ccService.getConfig().content.message = data[ 'cookie.message' ];
      this.ccService.getConfig().content.dismiss = data[ 'cookie.dismiss' ];
      this.ccService.getConfig().content.allow = data[ 'cookie.allow' ];
      this.ccService.getConfig().content.deny = data[ 'cookie.deny' ];
      this.ccService.getConfig().content.link = data[ 'cookie.link' ];
      this.ccService.getConfig().content.policy = data[ 'cookie.policy' ];

      this.ccService.destroy(); // remove previous cookie bar (with default messages)
      this.ccService.init( this.ccService.getConfig() ); // update config with translated messages
    } );

    // subscribe to cookieconsent observables to react to main events
    this.popupOpenSubscription = this.ccService.popupOpen$.subscribe(
      () => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - popupOpen' );
      } );

    this.popupCloseSubscription = this.ccService.popupClose$.subscribe(
      () => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - popupClose$' );
      } );

    this.initializeSubscription = this.ccService.initialize$.subscribe(
      ( event: NgcInitializeEvent ) => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - initialize$ event=', event );
      } );

    this.statusChangeSubscription = this.ccService.statusChange$.subscribe(
      ( event: NgcStatusChangeEvent ) => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - statusChange$ event=', event );
        if ( event.status === 'allow' ) {
          this.analytics.anonymiseIpAddress( false );
        }
        else {
          // By default anonymise IP, this way, if the code ever change within NgcCookieConsentService and I don't update this
          // I will still be GDPR compliant
          this.analytics.anonymiseIpAddress( true );
        }
      } );

    this.revokeChoiceSubscription = this.ccService.revokeChoice$.subscribe(
      () => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - revokeChoice$ event=', event );
      } );

    this.noCookieLawSubscription = this.ccService.noCookieLaw$.subscribe(
      ( event: NgcNoCookieLawEvent ) => {
        // you can use this.ccService.getConfig() to do stuff...
        this.logger.log( 'CookieService - noCookieLaw$ event=', event );
      } );

  }

  public ngOnDestroy() {
    this.logger.log( 'CookieService - ngOnDestroy' );

    // unsubscribe to cookieconsent observables to prevent memory leaks
    this.popupOpenSubscription.unsubscribe();
    this.popupCloseSubscription.unsubscribe();
    this.initializeSubscription.unsubscribe();
    this.statusChangeSubscription.unsubscribe();
    this.revokeChoiceSubscription.unsubscribe();
    this.noCookieLawSubscription.unsubscribe();
  }
}
