import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggerService } from './logger.service';
import { environment } from '@src/environments/environment';
import { first, map } from 'rxjs/operators';

@Injectable( {
  providedIn: 'root'
} )
export class HttpService {

  private readonly BACKEND_API = environment.BACKEND_API;

  constructor(
    private http: HttpClient,
    private logger: LoggerService
  ) {
  }

  public sendMessage( iParams ) {
    return this.postQuery( `${ this.BACKEND_API }send-message`, iParams );
  }

  public getQuery( iUrl, iResponseType: any = 'json', iParamRequest?: any ): Observable<any> {

    this.logger.log( 'HttpService - getQuery() - iUrl=', iUrl );

    const headers = new HttpHeaders( {
      'Content-Type': 'application/json',
    } );

    return this.http.get( iUrl, {
      headers,
      params: iParamRequest,
      observe: 'response',
      responseType: iResponseType,
    } )
    .pipe( first() ).pipe( map( ( res: any ) => {
        this.logger.log( 'HttpService - getQuery() - res=', res );

        return {
          status: res.status,
          response: res.body,
        };
      },
    ) );
  }

  public getBlobQuery( iUrl ): Observable<any> {
    return this.getQuery( iUrl, 'blob' );
  }


  public postQuery( iUrl: string, iBodyPostRequest?: {} ): Observable<any> {
    this.logger.log( 'postQuery iUrl=', iUrl );

    return this.http.post( iUrl, iBodyPostRequest, {
      observe: 'response',
    } )
    .pipe( first() ).pipe( map( ( res: any ) => {
        this.logger.log( 'HttpService - postQuery() - res=', res );

        return {
          status: res.status,
          response: res.body,
        };
      },
    ) );
  }
}
