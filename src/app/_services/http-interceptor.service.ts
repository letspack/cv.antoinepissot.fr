import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { StorageService } from '@app/_services/storage.service';
import { LoggerService } from '@app/_services/logger.service';
import { tap } from 'rxjs/operators';

@Injectable( {
  providedIn: 'root'
} )
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private storage: StorageService,
    private logger: LoggerService
  ) {
  }

  public intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

    // Request Interceptor
    const token = this.storage.getToken();
    if ( token ) {
      request = request.clone( {
        // setHeaders: {
        //   Authorization: `${ token }`,
        // },
        body: {
          ...request.body,
          token
        }
      } );
    }

    return next.handle( request ).pipe( tap( ( event: HttpEvent<any> ) => {

        // Response Interceptor
        if ( event instanceof HttpResponse ) {
          // do stuff with response if you want
          this.logger.log( `successful reply from the server for request=${ request.urlWithParams }` );
        }
      },
      ( responseError: any ) => {

        // ResponseError Interceptor
        if ( responseError instanceof HttpErrorResponse ) {
          this.logger.error( 'response in the catch: ', responseError );
          return throwError( responseError );
        }

      } ) ) as any;
  }

}
