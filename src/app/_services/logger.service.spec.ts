import { TestBed } from '@angular/core/testing';

import { LoggerService } from './logger.service';

export class MockLoggerService {
  public log() {}
  public warn() {}
  public info() {}
  public error() {}
  public getLogHistory() {
    return [];
  }
}


describe( 'LoggerService', () => {
  let service: LoggerService;

  beforeEach( () => {
    TestBed.configureTestingModule( {} );
    service = TestBed.get( LoggerService );

    spyOn( console, 'log' );
    spyOn( console, 'info' );
    spyOn( console, 'warn' );
    spyOn( console, 'error' );
  } );

  it( 'should be created', () => {
    expect( service ).toBeTruthy();
  } );

  it( 'addLog() && getLogHistory()', () => {
    service.logHistory = [];
    service.addLog( service.LOG_TYPE.log, 'Cesar' );
    expect( service.getLogHistory() ).toEqual( [ {
      type: service.LOG_TYPE.log,
      log: 'Cesar'
    } ] );

  } );

  it( 'log()', () => {
    spyOn( service, 'addLog' );

    service.log( 'Julio', 'Cesar' );

    expect( service.addLog ).toHaveBeenCalledWith( service.LOG_TYPE.log, JSON.stringify( [ 'Julio', 'Cesar' ] ) );
  } );

  it( 'warn()', () => {
    spyOn( service, 'addLog' );

    service.warn( 'Julio', 'Cesar' );

    expect( service.addLog ).toHaveBeenCalledWith( service.LOG_TYPE.warn, JSON.stringify( [ 'Julio', 'Cesar' ] ) );
  } );

  it( 'info()', () => {
    spyOn( service, 'addLog' );

    service.info( 'Julio', 'Cesar' );

    expect( service.addLog ).toHaveBeenCalledWith( service.LOG_TYPE.info, JSON.stringify( [ 'Julio', 'Cesar' ] ) );
  } );

  it( 'error()', () => {
    spyOn( service, 'addLog' );

    service.error( 'Julio', 'Cesar' );

    expect( service.addLog ).toHaveBeenCalledWith( service.LOG_TYPE.error, JSON.stringify( [ 'Julio', 'Cesar' ] ) );
  } );
} );
