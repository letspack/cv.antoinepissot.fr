import { TestBed } from '@angular/core/testing';

import { StorageService } from './storage.service';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { WindowRefService } from '@app/_services/window-ref.service';

describe( 'StorageService', () => {

  class MockWindowRefService {
    public nativeWindow = {
      localStorage: {
        setItem: () => {
        },
        getItem: () => {
          return `{ "token": "niceToken" }`;
        }
      }
    };
  }

  let service: StorageService;
  beforeEach( () => {
    TestBed.configureTestingModule( {
      providers: [
        { provide: WindowRefService, useClass: MockWindowRefService },
      ]
    } );

    service = TestBed.get( StorageService );
  } );

  it( 'setToken()', () => {
    spyOn( service.storage, 'setItem' );
    service.setToken( 'aToken' );
    expect( service.storage.setItem ).toHaveBeenCalled();
  } );

  it( 'getToken()', () => {
    spyOn( service.storage, 'getItem' ).and.callThrough();
    const token = service.getToken();
    expect( token ).toEqual( 'niceToken' );
    expect( service.storage.getItem ).toHaveBeenCalled();
  } );

} );
