import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from '@src/environments/environment';
import { LoggerService } from '@app/_services/logger.service';

declare var gtag: ( ...args ) => void;

@Injectable( {
  providedIn: 'root'
} )
export class AnalyticsService {

  constructor( private router: Router,
               private logger: LoggerService, ) {

  }

  public event( eventName: string, params: {} ) {
    gtag( 'event', eventName, params );
  }

  public init() {
    this.listenForRouteChanges();

    try {

      const script1 = document.createElement( 'script' );
      script1.async = true;
      script1.src = 'https://www.googletagmanager.com/gtag/js?id=' + environment.googleAnalyticsKey;
      document.head.appendChild( script1 );

      const script2 = document.createElement( 'script' );
      script2.innerHTML = `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '` + environment.googleAnalyticsKey + `', {'send_page_view': false});
      `;
      document.head.appendChild( script2 );
    }
    catch ( error ) {
      this.logger.error( 'Error appending google analytics, error=', error );
    }
  }

  public anonymiseIpAddress( iValue: boolean ) {

    // anonymize_ip needs to be set to undefined and not false to be de-activated
    gtag( 'config', environment.googleAnalyticsKey, {
      anonymize_ip: iValue ? iValue : undefined
    } );
  }

  private listenForRouteChanges() {
    this.router.events.subscribe( ( event ) => {
      if ( event instanceof NavigationEnd ) {
        gtag( 'config', environment.googleAnalyticsKey, {
          page_path: event.urlAfterRedirects,
        } );
        this.logger.log( 'Sending Google Analytics hit for route', event.urlAfterRedirects );
      }
    } );
  }
}
