import { TestBed } from '@angular/core/testing';

import { CookieService } from './cookie.service';
import { NgcCookieConsentService } from 'ngx-cookieconsent';
import { of } from 'rxjs';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { TranslateService } from '@ngx-translate/core';
import { AnalyticsService } from '@app/_services/analytics.service';

describe( 'CookieService', () => {

  class MockNgcCookieConsentService {
    public popupOpen$ = of({});
    public popupClose$ = of({});
    public initialize$ = of({});
    public statusChange$ = of({
      status: 'allow'
    });
    public revokeChoice$ = of({});
    public noCookieLaw$ = of({});

    public getConfig() {
      return {
        content: {}
      };
    }

    public destroy() {}
    public init() {}
  }

  class MockTranslateService {
    public get( iDataToTranslate: string[] ) {

      const translatedData = {};
      for ( const item of iDataToTranslate ) {
        translatedData[ item ] = `${item} TRANSLATED`;
      }

      return of( translatedData );
    }
  }

  class MockAnalyticsService {
    public anonymiseIpAddress() {
    }
  }

  let service: CookieService;
  let analytics: AnalyticsService;

  beforeEach( () => {
    TestBed.configureTestingModule( {
      providers: [
        { provide: NgcCookieConsentService, useClass: MockNgcCookieConsentService },
        { provide: LoggerService, useClass: MockLoggerService },
        { provide: TranslateService, useClass: MockTranslateService },
        { provide: AnalyticsService, useClass: MockAnalyticsService },
      ]
    } );

    service = TestBed.get( CookieService );
    analytics = TestBed.get( AnalyticsService );

  } );

  it( 'should be created', () => {
    expect( service ).toBeTruthy();
  } );

  it( 'init()', () => {
    spyOn( analytics, 'anonymiseIpAddress' );
    service.init();
    expect( analytics.anonymiseIpAddress ).toHaveBeenCalledWith( false );
  } );
} );
