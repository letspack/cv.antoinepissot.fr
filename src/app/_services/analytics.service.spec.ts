import { TestBed } from '@angular/core/testing';

import { AnalyticsService } from './analytics.service';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { NavigationEnd, Router } from '@angular/router';
import { of } from 'rxjs';

describe('AnalyticsService', () => {

  class MockRouter {
    public events = of( new NavigationEnd(1, 'home', 'home') );
  }

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: Router, useClass: MockRouter },
      { provide: LoggerService, useClass: MockLoggerService },
    ]
  }));

  it('should be created', () => {
    const service: AnalyticsService = TestBed.get(AnalyticsService);
    expect(service).toBeTruthy();
  });
});
