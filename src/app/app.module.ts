import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { NgcCookieConsentConfig, NgcCookieConsentModule } from 'ngx-cookieconsent';
import { HttpInterceptorService } from '@app/_services/http-interceptor.service';
import { ENV_PROVIDERS, bugsnagClient, environment } from '@src/environments/environment';
import { LayoutComponent } from './components/container/layout/layout.component';
import { HomeComponent } from './components/container/home/home.component';
import { ProfileComponent } from './components/presentation/profile/profile.component';
import { ExpertiseComponent } from './components/presentation/expertise/expertise.component';
import { NavbarComponent } from './components/presentation/navbar/navbar.component';
import { APP_CONSTANT, APP_DI_CONSTANT } from '@app/app.constant';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { cookieConfig } from '@app/_services/cookie.service';

import { BugsnagErrorHandler } from '@bugsnag/plugin-angular';


export function createTranslateLoader( http: HttpClient ) {
  return new TranslateHttpLoader( http, './assets/i18n/', '.json' );
}

export function errorHandlerFactory() {
  return new BugsnagErrorHandler( bugsnagClient );
}

import { ExperienceComponent } from './components/presentation/experience/experience.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomAngularMaterialModule } from '@app/app.angular-material.module';
import { LogoComponent } from './components/presentation/logo/logo.component';
import { JobHeaderComponent } from './components/presentation/experience/job-header/job-header.component';
import { JobDescriptionComponent } from './components/presentation/experience/job-description/job-description.component';
import { SanitizeHtmlPipe } from '@app/_pipes/sanitizeHtml';
import { EducationComponent } from './components/presentation/education/education.component';
import { InterestsComponent } from './components/presentation/interests/interests.component';
import { LanguagesComponent } from './components/presentation/languages/languages.component';
import { ContactComponent } from './components/container/contact/contact.component';
import { CarrouselPhotosComponent } from './components/presentation/carrousel-photos/carrousel-photos.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule( {
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    ProfileComponent,
    ExpertiseComponent,
    NavbarComponent,
    ExperienceComponent,
    LogoComponent,
    JobHeaderComponent,
    JobDescriptionComponent,
    SanitizeHtmlPipe,
    EducationComponent,
    InterestsComponent,
    LanguagesComponent,
    ContactComponent,
    CarrouselPhotosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    CustomAngularMaterialModule,
    DeviceDetectorModule.forRoot(),
    TranslateModule.forRoot( {
      loader: {
        provide: TranslateLoader,
        useFactory: ( createTranslateLoader ),
        deps: [ HttpClient ]
      }
    } ),
    NgcCookieConsentModule.forRoot( cookieConfig ),
    StoreModule.forRoot( reducers, { metaReducers } ),
    ServiceWorkerModule.register( 'ngsw-worker.js', { enabled: environment.production } ),
  ],
  providers: [
    {
      provide: ErrorHandler,
      useFactory: errorHandlerFactory
    },
    {
      provide: APP_CONSTANT,
      useValue: APP_DI_CONSTANT
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
    ...ENV_PROVIDERS
  ],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
