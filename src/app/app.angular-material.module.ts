import { NgModule } from '@angular/core';

import {
  MatDividerModule,
  MatExpansionModule,
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
} from '@angular/material';



@NgModule( {
  imports: [
    MatExpansionModule,
    MatDividerModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
  ],
  providers: [
  ],
  exports: [
    MatExpansionModule,
    MatDividerModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
  ],
} )
export class CustomAngularMaterialModule {
}
