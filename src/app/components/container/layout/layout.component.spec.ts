import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutComponent } from './layout.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MenuItemClicked } from '@app/reducers/menuItem/menuItem.actions';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

describe('LayoutComponent', () => {

  let store: MockStore<{ currentMenuItem: null }>;
  const initialState = { currentMenuItem: null };

  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutComponent ],
      providers: [
        provideMockStore({ initialState }),
        { provide: LoggerService, useClass: MockLoggerService },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();

    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('handleMenuItemClicked()', () => {
    spyOn( store, 'dispatch' );

    const dataAction = new MenuItemClicked( MenuItemType.CONTACT );

    component.handleMenuItemClicked( MenuItemType.CONTACT );

    expect( store.dispatch ).toHaveBeenCalledWith( dataAction );
  });
});
