import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoggerService } from '@app/_services/logger.service';
import { HttpService } from '@app/_services/http.service';
import { finalize } from 'rxjs/operators';

export enum STATUS_SEND_MESSAGE {
  NOT_SENT = 1,
  SUCCESS,
  ERROR,
  ERROR_THROTTLE
}

@Component( {
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: [ './contact.component.scss' ]
} )
export class ContactComponent implements OnInit {


  public contactForm: FormGroup;

  public STATUS_SEND_MESSAGE = STATUS_SEND_MESSAGE;
  public statusSendMessage: STATUS_SEND_MESSAGE = STATUS_SEND_MESSAGE.NOT_SENT;

  constructor(
    private logger: LoggerService,
    private fb: FormBuilder,
    private http: HttpService ) {
  }

  ngOnInit() {

    this.contactForm = this.fb.group( {
      email: [ '', [ Validators.required, Validators.email ] ],
      subject: [ '', Validators.required ],
      content: [ '', Validators.required ],
    } );
  }

  public onSubmit() {
    this.logger.log( 'onSubmit() contactForm.value=', this.contactForm.value );
    if ( this.contactForm.valid ) {
      this.logger.log( 'onSubmit() form is VALID' );

      const params = {
        fromEmail: this.contactForm.value.email,
        subject: this.contactForm.value.subject,
        message: this.contactForm.value.content,
      };

      this.http.sendMessage( params )
      .pipe( finalize( () => {
        setTimeout( () => {
          this.statusSendMessage = STATUS_SEND_MESSAGE.NOT_SENT;
        }, 10000 );
      } ) )
      .subscribe( ( response ) => {
          this.logger.log( 'sendMessage() success sending request to Back End response=', response );
          this.statusSendMessage = STATUS_SEND_MESSAGE.SUCCESS;
          this.contactForm.reset();
        },
        ( error ) => {
          this.logger.error( 'sendMessage() error sending request to Back End error=', error );
          if ( error.status === 429 ) {
            // User tried too many times (throttle is set to 5 request per minute on back end)
            this.statusSendMessage = STATUS_SEND_MESSAGE.ERROR_THROTTLE;
          }
          else {
            this.statusSendMessage = STATUS_SEND_MESSAGE.ERROR;
          }

        } );

    }
    else {
      this.logger.log( 'onSubmit() form is NOT VALID' );
    }
  }

}
