import { Component, Input, OnInit } from '@angular/core';
import { SingleExperienceInterface } from '@app/components/presentation/experience/experience.component';

@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss']
})
export class JobDescriptionComponent implements OnInit {

  @Input() public jobExperience: SingleExperienceInterface;
  @Input() public isSmallScreen: boolean;

  constructor() { }

  ngOnInit() {
  }

}
