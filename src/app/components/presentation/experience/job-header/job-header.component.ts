import { Component, Input, OnInit } from '@angular/core';
import { SingleExperienceInterface } from '@app/components/presentation/experience/experience.component';

@Component({
  selector: 'app-job-header',
  templateUrl: './job-header.component.html',
  styleUrls: ['./job-header.component.scss']
})
export class JobHeaderComponent implements OnInit {

  @Input() public jobExperience: SingleExperienceInterface;

  constructor() { }

  ngOnInit() {
  }

}
