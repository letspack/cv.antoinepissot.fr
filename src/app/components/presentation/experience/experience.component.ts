import { Component, OnInit } from '@angular/core';
import {
  LogoInfo,
  LogoSize,
  LogoType,
} from '@app/components/presentation/logo/logo.component';

export interface SingleExperienceInterface {
  company: {
    logo: LogoInfo;
    url: string;
    name: string;
  };
  jobTitle: string;
  dates: string;
  jobSummary: string;
  description: string[];
  logos: LogoInfo[];
  location: {
    name: string;
    googleMapUrl: string;
  };
}

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss'],
})
export class ExperienceComponent implements OnInit {
  /* tslint:disable:max-line-length */
  public experiences: SingleExperienceInterface[] = [
    {
      company: {
        logo: {
          type: LogoType.Lavanda,
          customCssClass: 'img-fluid size-70',
        },
        url: 'https://potentialife.com',
        name: 'Potentialife',
      },
      location: {
        name: 'Toulouse',
        googleMapUrl: 'https://goo.gl/maps/eBRSotr7H48NrV3NA',
      },
      jobTitle: 'Lead Front-end developer',
      dates: 'Jul 2019 - current',
      jobSummary:
        'Lavanda enables residential communities to optimize building utilization and increase net operating income (NOI) with a 100% flexible short and medium-term rental solution',
      description: [
        `<strong>Created new set of functionalities</strong> in order to help sell the product to different actors on the market (going from short term rental to mid-long term rental)`,
        `<strong>Led regular retrospective events</strong> in order to increase team efficiency and happiness`,
        `<strong>Set up AWS infrastructure for front-end development</strong> reducing cost by 80% and enabled lazy-loading implementation`,
      ],
      logos: [
        {
          type: LogoType.Angular,
          size: LogoSize.S,
        },
        {
          type: LogoType.TypeScript,
          size: LogoSize.S,
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS,
        },
        {
          type: LogoType.HTML,
          size: LogoSize.S,
        },
        {
          type: LogoType.CSS,
          size: LogoSize.S,
        },
        {
          type: LogoType.SCSS,
          size: LogoSize.S,
        },
        {
          type: LogoType.AWS,
          size: LogoSize.S,
        },
      ],
    },
    {
      company: {
        logo: {
          type: LogoType.Potentialife,
          customCssClass: 'img-fluid size-70',
        },
        url: 'https://potentialife.com',
        name: 'Potentialife',
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42',
      },
      jobTitle: 'Chief Technology Officer',
      dates: 'Jan 2018 - Jul 2019',
      jobSummary:
        'Potentialife transforms organisations by changing behaviours at scale. Using our technology platform and behavioural-science programmes, we help people become happier and more productive leaders. Participants login to a website (comparable to udacity) where they watch videos, do exercises, answer survey…',
      description: [
        `<strong>Recruited and developed 5-person tech team</strong> by holding bi-weekly one-to-one and quarterly performance reviews, conducting on the job coaching and sharing best practices to upskill the team.`,
        `<strong>Created and maintained new functionalities</strong> in Angular and Angularjs websites to increase user engagement by 50% and decrease manual delivery work by 30% for my colleagues.`,
        `<strong>Set up daily stand-up, weekly sprint planning, monthly retrospective</strong> leveraging Agile methodology to decrease delivery time by 50% and increase on-time delivery from 10% to 90%.`,
        `<strong>Organised backlog and created user stories</strong> based on monthly meeting with stakeholders.`,
        `<strong>Led</strong> the implementation of GDPR to meet May 2018 deadline without degrading user experience.`,
      ],
      logos: [
        {
          type: LogoType.Angular,
          size: LogoSize.S,
        },
        {
          type: LogoType.TypeScript,
          size: LogoSize.S,
        },
        {
          type: LogoType.AngularJS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS,
        },
        {
          type: LogoType.HTML,
          size: LogoSize.S,
        },
        {
          type: LogoType.CSS,
          size: LogoSize.S,
        },
        {
          type: LogoType.PHP,
          size: LogoSize.XS,
        },
        {
          type: LogoType.Laravel,
          size: LogoSize.S,
        },
        {
          type: LogoType.AWS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Docker,
          size: LogoSize.S,
        },
      ],
    },
    {
      company: {
        logo: {
          type: LogoType.Potentialife,
          customCssClass: 'img-fluid size-70',
        },
        url: 'https://potentialife.com',
        name: 'Potentialife',
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42',
      },
      jobTitle: 'Lead Front-end Developer',
      dates: 'Jan 2017 - Dec 2017',
      jobSummary: '',
      description: [
        `<strong>Managed 2 interns and 3 contractors</strong> to increase the stability of our infrastructure and the speed of delivery.`,
        `<strong>Designed, created and built 2 websites</strong> (user facing and user management) from scratch using AngularJS and Angular to improve user experience which doubled user retention.`,
        `<strong>Set up CI/CD</strong> on Bitbucket/AWS S3 and Cloudfront for all front-end websites to decrease deployment time by 50%.`,
      ],
      logos: [
        {
          type: LogoType.Angular,
          size: LogoSize.S,
        },
        {
          type: LogoType.TypeScript,
          size: LogoSize.S,
        },
        {
          type: LogoType.AngularJS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS,
        },
        {
          type: LogoType.PHP,
          size: LogoSize.XS,
        },
        {
          type: LogoType.Laravel,
          size: LogoSize.S,
        },
        {
          type: LogoType.AWS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Webpack,
          size: LogoSize.S,
        },
        {
          type: LogoType.Docker,
          size: LogoSize.S,
        },
      ],
    },
    {
      company: {
        logo: {
          type: LogoType.Amadeus,
          customCssClass: 'img-fluid size-50',
        },
        url: 'https://amadeus.com/',
        name: 'Amadeus',
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42',
      },
      jobTitle: 'AngularJs Developer',
      dates: 'Jul 2015 - Dec 2016',
      jobSummary:
        'Altea Customer Management & Flight Management - Amadeus is the world leading solution to automate key airport processes for airlines from check-in to departure.',
      description: [
        `<strong>Designed and implemented</strong> a website from scratch using AngularJS to increase the usability of Flight History application which reduce average load time by 200%.`,
        `<strong>Set up CI</strong> using gulp / git / jasmine /protractor / eslint to increase code quality and detect problems more quickly.`,
        `<strong>Worked in a full agile environment</strong> entrusted with sole responsibility for leading customer engagement in sprints.`,
      ],
      logos: [
        {
          type: LogoType.AngularJS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS,
        },
        {
          type: LogoType.HTML,
          size: LogoSize.S,
        },
        {
          type: LogoType.CSS,
          size: LogoSize.S,
        },
        {
          type: LogoType.MongoDB,
          size: LogoSize.S,
        },
        {
          type: LogoType.Git,
          size: LogoSize.XS,
        },
        {
          type: LogoType.Gulp,
          size: LogoSize.S,
        },
        {
          type: LogoType.Jasmine,
          size: LogoSize.S,
        },
      ],
    },
    {
      company: {
        logo: {
          type: LogoType.Letspack,
          customCssClass: 'img-fluid',
        },
        url: null,
        name: 'Letspack',
      },
      location: {
        name: 'World',
        googleMapUrl: 'https://earth.google.com/web/',
      },
      jobTitle: 'Sabbatical Year',
      dates: 'Jan 2014 - Dec 2014',
      jobSummary:
        'Followed my passion for travel and culture by backpacking around 18 countries globally',
      description: [],
      logos: [
        {
          type: LogoType.Chile,
          size: LogoSize.S,
        },
        {
          type: LogoType.Peru,
          size: LogoSize.S,
        },
        {
          type: LogoType.Ecuador,
          size: LogoSize.S,
        },
        {
          type: LogoType.Colombia,
          size: LogoSize.S,
        },
        {
          type: LogoType.Panama,
          size: LogoSize.S,
        },
        {
          type: LogoType.Guatemala,
          size: LogoSize.S,
        },
        {
          type: LogoType.Belize,
          size: LogoSize.S,
        },
        {
          type: LogoType.Mexico,
          size: LogoSize.S,
        },
        {
          type: LogoType.US,
          size: LogoSize.S,
        },
        {
          type: LogoType.Japan,
          size: LogoSize.S,
        },
        {
          type: LogoType.Taiwan,
          size: LogoSize.S,
        },
        {
          type: LogoType.HongKong,
          size: LogoSize.S,
        },
        {
          type: LogoType.Macau,
          size: LogoSize.S,
        },
        {
          type: LogoType.Vietnam,
          size: LogoSize.S,
        },
        {
          type: LogoType.Laos,
          size: LogoSize.S,
        },
        {
          type: LogoType.Myanmar,
          size: LogoSize.S,
        },
        {
          type: LogoType.Singapore,
          size: LogoSize.S,
        },
        {
          type: LogoType.Thailand,
          size: LogoSize.S,
        },
      ],
    },
    {
      company: {
        logo: {
          type: LogoType.Amadeus,
          customCssClass: 'img-fluid size-50',
        },
        url: 'https://amadeus.com/',
        name: 'Amadeus',
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42',
      },
      jobTitle: 'C++ Back-end Developer',
      dates: 'Jan 2010 - Jan 2014',
      jobSummary: '',
      description: [
        `<strong>Designed, coded and delivered multiple global projects</strong> in multi-team and multi-site remote environments.`,
        `<strong>Resolved mission critical server</strong> issues as part of a 30min response team to insure 24/7 availability of mission critical applications used by more than 50 airlines across the globe.`,
        `<strong>Rewrote DB queries </strong> to access relevant data more efficiently which reduced load time of passenger lists by 30%.`,
        `<strong>Designed and implemented </strong> “back up” software to intercept and duplicate all DB queries from Oracle to NoSql DB.`,
      ],
      logos: [
        {
          type: LogoType.AngularJS,
          size: LogoSize.S,
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS,
        },
        {
          type: LogoType.CPP,
          size: LogoSize.XS,
        },
        {
          type: LogoType.MongoDB,
          size: LogoSize.S,
        },
        {
          type: LogoType.Git,
          size: LogoSize.XS,
        },
        {
          type: LogoType.Gulp,
          size: LogoSize.S,
        },
        {
          type: LogoType.Jasmine,
          size: LogoSize.S,
        },
      ],
    },
  ];

  constructor() {}

  public ngOnInit() {}
}
