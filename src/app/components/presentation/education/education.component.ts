import { Component, OnInit } from '@angular/core';
import { LogoType } from '@app/components/presentation/logo/logo.component';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  public readonly education = [
    {
      logoInfo: {
        type: LogoType.INSA,
        customCssClass: 'height-50-px'
      },
      dates: '2004 - 2009',
      description: 'Master in computer science',
      website: {
        nameLink: 'website',
        url: 'https://www.insa-toulouse.fr/en/index.html'
      },
      location: {
        name: 'Toulouse',
        googleMapUrl: 'https://goo.gl/maps/qTyC7yXZSRP2'
      }
    },
    {
      logoInfo: {
        type: LogoType.LeicesterUniversity,
        customCssClass: 'height-50-px'
      },
      dates: '2008',
      description: 'Erasmus, exchange semester',
      website: {
        nameLink: 'website',
        url: 'https://le.ac.uk/'
      },
      location: {
        name: 'Leicester',
        googleMapUrl: 'https://goo.gl/maps/84wRrE8APXy'
      },
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
