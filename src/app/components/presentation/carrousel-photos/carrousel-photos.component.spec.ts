import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrouselPhotosComponent } from './carrousel-photos.component';
import { Pipe, PipeTransform } from '@angular/core';

describe('CarrouselPhotosComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockTranslatePipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  let component: CarrouselPhotosComponent;
  let fixture: ComponentFixture<CarrouselPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CarrouselPhotosComponent,
        MockTranslatePipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrouselPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
