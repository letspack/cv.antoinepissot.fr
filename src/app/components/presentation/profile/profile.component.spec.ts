import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';

describe('ProfileComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockTranslatePipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  class MockTranslateService {
    public get( iDataToTranslate: string ) {
      return of( [ iDataToTranslate ] );
    }
  }

  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  let store: MockStore<{ currentMenuItem: null }>;
  const initialState = { currentMenuItem: null };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        MockTranslatePipe,
      ],
      providers: [
        provideMockStore({ initialState }),
        { provide: TranslateService, useClass: MockTranslateService },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();

    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
