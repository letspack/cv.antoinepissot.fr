import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestsComponent } from './interests.component';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';

describe('InterestsComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockPipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  let component: InterestsComponent;
  let fixture: ComponentFixture<InterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        InterestsComponent,
        MockPipe
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
