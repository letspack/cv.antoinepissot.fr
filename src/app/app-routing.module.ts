import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '@app/components/container/layout/layout.component';
import { HomeComponent } from '@app/components/container/home/home.component';

const routes: Routes = [
  {
    canActivate: [],
    component: LayoutComponent,
    path: '',
    children: [
      {
        component: HomeComponent,
        path: '',
      }
    ]
  }
];

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
