import { Component, OnInit } from '@angular/core';
import { LoggerService } from '@app/_services/logger.service';
import { saveAs } from 'file-saver';
import { environment } from '@src/environments/environment';


import {TranslateService} from '@ngx-translate/core';
import { AnalyticsService } from '@app/_services/analytics.service';
import { CookieService } from '@app/_services/cookie.service';


@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
} )
export class AppComponent implements OnInit {

  constructor(
    private translate: TranslateService,
    private analytics: AnalyticsService,
    private cookieService: CookieService,
    private logger: LoggerService,
  ) {
  }

  public ngOnInit() {

    /* tslint:disable no-trailing-whitespace */
    this.logger.error(
      `%c       ###################################################################################################       
                      TONIO CV - ${ environment.TARGET_DEPLOYMENT }: v${ environment.VERSION }                                               
       ###################################################################################################       `,
      'background: #bc0000; color: #ffffff' );
    /* tslint:enable no-trailing-whitespace */

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use('en');

    this.analytics.init();

    this.cookieService.init();
  }

}
